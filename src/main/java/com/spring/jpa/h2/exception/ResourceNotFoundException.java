package com.spring.jpa.h2.exception;

public class ResourceNotFoundException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public ResourceNotFoundException(String msg) {
        super(msg);
    }
}

class CertainException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public CertainException(String msg) {
        super(msg);
    }
}
